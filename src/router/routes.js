import tableComponent from '../components/tableComponent';
import newPost from '../components/newPost';
import editPopup from '../components/editPopup';
import deletePopup from '../components/deletePopup';

export default [
    {
      path: '/',
      name: 'home',
      component: tableComponent,
      children: [
        {
          path: '/new-post',
          name: 'newPost',
          component: newPost
        },
        {
          path: '/edit-post/:id',
          name: 'edit-post',
          component: editPopup
        },
        {
          path: '/del-post/:id',
          name: 'del-post',
          component: deletePopup
        }
      ]
    }
];