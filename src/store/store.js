import Vue from 'vue';
import Vuex from 'vuex';

import postArray from '../components/postArray';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    postArray: postArray
  },
  mutations: {
    
    /**
     * Создание нового поста
     * @param {array} addPost 
     */
    mutationNewPost(state, addPost){
      
      state.postArray.push(addPost);
      
      // Результат
      console.log(state.postArray);
    },

    mutationEditPost(state, { needItem, editPost } ){

      needItem.title = editPost.title;
      needItem.content = editPost.content;

      // Результат
      console.log(state.postArray);
    },

  
    mutationDeletePost(state, deletePostId) {
      
      state.postArray.splice(deletePostId, 1);

      // Результат 
      console.log(state.postArray);
    }

  },
  actions: {

    /**
     * Перезаисываем данные поста
     * @param {array} editPost 
     */
    actionEditPost(state, editPost) {
      
      this.state.postArray.forEach((needItem) => { 
        if (needItem.id == editPost.id) {

          // Есть ли отличие
          if (needItem.title != editPost.title || needItem.content != editPost.content) {

            state.commit('mutationEditPost', { needItem, editPost } );
          }
        }
      }); 

    },  

    /**
     * Удаление поста 
     * @param {string} deletePostId 
     */
    actionDeletePost(state, deletePostId) {
      /**
       * Поскольку удалять нужно по ключю, приходится узнавать какой ключ у удаляемого поста.
       */
      var count = 0;
      var length = this.state.postArray.length;

      while (count < length) {

        var post = this.state.postArray[count];

        if (post["id"] == deletePostId) {

          state.commit('mutationDeletePost', count);

          // Завершаем цикл 
          count = length;
        }
        count++;
      }
    }

  }
});