import Vue from 'vue';
import App from './App.vue';
import Router from 'vue-router';
import routes from './router/routes.js';
import VueTables from 'vue-tables-2';
import store from './store/store';

Vue.use(Router);
Vue.use(VueTables.ClientTable);

Vue.config.productionTip = false;

export const router = new Router({
  mode: 'history',
  history: 'true',
  routes
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
